//
//  WebMix/noobyTCP/tcp.hpp	RGD@KYOTO-U
//
//NoobyTCP
//		Enable the communication between your C++ program and browser!
//使い方
//		TCP server(N);									Create interface at port N. You can increase number of threads M by
//														TCP server(N,M). Default value is M=3
//		auto& conn=server.Protocol["raw"];				Add sub-protocol "raw" (you can use any text)
//														conn is of type TCP::Protocol
//		conn.on_open=[...](TCP::Session&session){...};	You may add additional callback at open event
//		conn.on_close=[...](TCP::Session&session){...};	You may add additional callback at close event
//		conn.on_error=[...](TCP::Session&session){...};	You may add additional callback at error event
//		conn.on_mesage=[...](TCP::Session& session,const std::string& message){....};
//														You must add callback at message event
//		server.start();
//			You can access your C++ program by URI=http://IP-ADDRESS:port
//				from javascript: Websocket(URI,"raw");	You can specify the channel "raw"
//				from C++: see client.cpp
//			You can use any number of sub-protocol (see test_server.cpp)
//		In the calback on_message(), you can use session.send() e.g.
//			conn.on_mesage=[](TCP::Session& session,const std::string& message){
//				....
//				session.send(std::string("reply to client"));
//				....
//			};
//----以前のバージョンとの違い
//1. サブプロトコルを作成する命令が endpoint()からProtocol()に変更
//		旧	auto& conn=server.endpoint("/raw.*/");	なぜか正規表現
//		新	auto& conn=server.Protocols["raw"];
//
//2. コールバックの引数が変更
//		conn.on_message=[]
//		旧	conn.on_message=[...](std::shared_ptr<TCP::Connection> connection,
//			std::shared_ptr<TCP::Message> message){...}
//		新	conn.on_message=[...](TCP::Session& settion,const std::string& message){...}
//
//3. コールバック中のクライアント送信が非同期から同期に変更(非同期にする意義が不明のため)
//4. コールバック中でクライアント送信関数は, TCPオブジェクトではなく, Sessionオブジェクトに付属
//		旧	server.send(conn,message);		serverのconnプロトコル通信でmessageを送信
//		新	session.send(message);
//
#ifndef tcp_hpp
#define tcp_hpp
#define BOOST_NO_CXX98_FUNCTION_BASE //Boost1.73ではboost.program_optionsがバグっておる
#include <boost/io/quoted.hpp>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <map>
#include <boost/algorithm/string.hpp>		//boost::erase_all
#include <boost/asio.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/strand.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <log4cplus/socketappender.h>		//ネットワークロガー
#include <log4cplus/configurator.h>			//ローカルロガー
#include <log4cplus/spi/loggingevent.h>
#include <log4cplus/loggingmacros.h>
#include <log4cplus/logger.h>
#include <boost/filesystem.hpp>
class Check_Port {
private:
    boost::asio::io_context ioc_;
	boost::asio::ip::tcp::socket sock_;
	unsigned long port_;
public:
	Check_Port(unsigned long port);
	unsigned long GetPort();
};
class TCP {
    log4cplus::Logger& log_;
public:
	class Session;
	//プトトコル	クライアント(ブラウザ)との対話手順を規定するクラス
	class Protocol {
	public:
		bool wait_message;	//trueであれば, 開始直後にメッセージを待つ
		std::string name;
		std::function<void(Session& session)> on_open;
		std::function<void(Session& session)> on_close;
		std::function<void(Session& session,const boost::system::error_code& ec)> on_error;
		std::function<void(Session& session,const std::string&)> on_message;
		Protocol(){
			wait_message=true;
		};
	};
	//対話 Session: 1つのProtocolに対し, 多数のSessionが発生する
	class Session : public std::enable_shared_from_this<Session> {
	public:
		boost::asio::ip::tcp::endpoint remote_endpoint;
		boost::asio::ip::tcp::endpoint local_endpoint;
	private:
		TCP& parent_;
		boost::beast::http::request<boost::beast::http::string_body> req_;//session headers
		Protocol& protocol_;
		boost::beast::websocket::stream<boost::beast::tcp_stream> ws_;
		boost::beast::flat_buffer buffer_;
		void Fail(boost::beast::error_code ec, char const* what);
		void on_run();// Start the asynchronous operation
		void on_accept(boost::beast::error_code ec);
		void do_read();// Read a message into our buffer
		void on_read(boost::beast::error_code ec, std::size_t bytes_transferred);
		void on_write(boost::beast::error_code ec, std::size_t bytes_transferred);
	public:
		explicit Session(boost::asio::ip::tcp::socket&& socket,
			boost::beast::http::request<boost::beast::http::string_body>& req,
			Protocol& proto,TCP& parent)
			:
			remote_endpoint(socket.lowest_layer().remote_endpoint()),
			local_endpoint(socket.lowest_layer().local_endpoint()),
			ws_(std::move(socket)),req_(req),protocol_(proto),parent_(parent)
		{
		};
		~Session(){};
		void Close();
		void Run();// Get on the correct executor
		void Read();//非同期読み込みを実施(読み込んだら, プロトコルのon_messageが実行される)
		void Send(const std::string& message);//同期書き込みを実施
	};
private:
    boost::asio::io_context ioc_;
	unsigned int threads_;
	//着信クラス Listener
	class Listener : public std::enable_shared_from_this<Listener> {
		TCP& parent_;
		boost::asio::io_context& ioc_;
		boost::asio::ip::tcp::acceptor acceptor_;
		boost::beast::error_code ec;
		void do_accept();	// The new connection gets its own strand
		void on_accept(boost::beast::error_code ec, boost::asio::ip::tcp::socket socket);
	public:
		Listener(boost::asio::io_context& ioc, boost::asio::ip::tcp::endpoint endpoint,TCP& parent);
		void run(); 		// Start accepting incoming connections
	};
	std::map<std::shared_ptr<Session>,Protocol*> sessions_;
public:
	std::map<std::string,Protocol> Protocols;			//Add sub-protocol here e.g. auto& name=TCPobj.Protocols["name"];
    TCP(log4cplus::Logger& logger,unsigned short port,unsigned short threads=3);
	std::string list();									//Obtain the list of active sessions
	void start(std::string NDC_layer=std::string(""));	//Start communication
	void stop();										//Stop communication
};
#endif
