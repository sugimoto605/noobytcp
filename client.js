var SOCK=[];
$(function(){
	$('.channel').append($('<option selected>').html("echo").val("echo"));
	$('.channel').append($('<option>').html("cmd").val("cmd"));
	$('.channel').append($('<option>').html("slowpoke").val("slowpoke"));
	$('.channel').append($('<option>').html("chk").val("chk"));
	$('.channel').append($('<option>').html("write").val("write"));
	$('.channel').append($('<option>').html("none").val("none"));
	$('.channel').append($('<option>').html("--").val(""));
	$('.start').click(function(){
		var _id=this.id.toString().substring(2);
		if (this.value=="connect")
		{
			var URI='ws://'+$('#server').val()+':'+$('#port').val();
			//Open WebSocket(URI,["protocol_1","protocol_2",....])	open URI with sub-protocols
			if ($('#ch'+_id).val()!="")
				SOCK[_id]=new WebSocket(URI,$('#ch'+_id).val());
			else
				SOCK[_id]=new WebSocket(URI);
			SOCK[_id].onopen=function(){
				var _now=new Date();
				$('#text'+_id).append(_now.toLocaleTimeString()+" "+URI+" opened at ch"+_id+'\n');
				$('#sw'+_id).val('close');
				$('.id'+_id).attr('disabled',false);
			};
			SOCK[_id].onerror=function(){
				var _now=new Date();
				$('#text'+_id).append(_now.toLocaleTimeString()+" "+URI+" error at ch"+_id+'\n');
			};
			SOCK[_id].onclose=function(){
				var _now=new Date();
				$('#text'+_id).append(_now.toLocaleTimeString()+" closed at ch"+_id+'\n');
				$('#sw'+_id).val('connect');
				$('.id'+_id).attr('disabled',true);
			};
			SOCK[_id].onmessage=function(reply){
				var _now=new Date();
				$('#text'+_id).append(_now.toLocaleTimeString()+" >> "+reply.data+'\n');
			};
		}
		else
		{
			$('#input'+_id).val('');
			SOCK[_id].close(1000);
		}
	});
	$('.xmit').click(function(){
		var _now=new Date();
		var _id=this.id.toString().substring(4);
		var _msg=$('#input'+_id).val();
		$('#text'+_id).append(_now.toLocaleTimeString()+" << "+_msg+'\n');
		SOCK[_id].send(_msg);
	});
});
