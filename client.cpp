//	NoobyTCP websocket client example
#include <iostream>
#include <string>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/tokenizer.hpp>

class CLIENT {
    boost::asio::io_context ioc;
	boost::asio::ip::tcp::resolver resolver_;
	boost::beast::websocket::stream<boost::asio::ip::tcp::socket> ws_;
	boost::asio::ip::tcp::resolver::results_type results_;
	std::string host_;
public:
	bool connected=false;
	CLIENT(std::string& host,std::string& port):resolver_{ioc},ws_{ioc},host_{host}
	{
		try
		{
			results_=resolver_.resolve(host,port);
		}
		catch(std::exception& e)
		{
			throw(std::runtime_error("Error in resolve."));
		}
		std::cout << "Host IP is";
		for(auto it=results_.begin();it!=results_.end();it++) std::cout << " " << it->endpoint().address().to_string();
		std::cout << std::endl;
	};
	void close()
	{
		ws_.close(boost::beast::websocket::close_code::normal);
		connected=false;
	};
	void connect(std::string protocol,std::string target)
	{
		boost::asio::connect(ws_.next_layer(),results_.begin(),results_.end());
        ws_.set_option(boost::beast::websocket::stream_base::decorator(
            [protocol](boost::beast::websocket::request_type& req)
            {
                req.set(boost::beast::http::field::sec_websocket_protocol,protocol.c_str());
                req.set(boost::beast::http::field::user_agent,std::string(BOOST_BEAST_VERSION_STRING)+" websocket-client-sync");
            }));
        ws_.handshake(host_, target);
		connected=true;
	};
	std::string send_get(std::string message)
	{
		std::cout<<"<< "<<message<<std::endl;
		ws_.write(boost::asio::buffer(message));
		std::cout<<"Waiting reply...."<<std::endl;
		boost::beast::flat_buffer buffer;
		ws_.read(buffer);
		std::string reply=boost::beast::buffers_to_string(buffer.data());
		return reply;
	};
};

int main(int argc, char** argv)
{
    if(argc != 2)
    {
        std::cerr <<
            "Usage: client host:port\n" <<
            "Example:\n" <<
            "    client 10.249.229.225:59639" << std::endl;
        return EXIT_FAILURE;
    }
    std::string ARG1(argv[1]);
	size_t n_host=ARG1.find(":");
	size_t n_port=ARG1.find("/");
	std::string host=ARG1.substr(0,n_host);
    std::string port=ARG1.substr(n_host+1);
	CLIENT cl(host,port);
	while (!std::cin.fail())
	{
		std::cout << ": ";
		std::string command;
		std::getline(std::cin,command);
		if (command=="") command="h";
		boost::tokenizer<> tokens(command);
		auto tok=tokens.begin();
		if (*tok=="h")
		{
			std::cout << "h: Show yelp\nopen channel: Open specified channel\nclose: Close\nold channel: Open old style channel\nq: Quit\nsend message: Send message"<<std::endl;
		}
		else if (*tok=="close")
		{
			if (cl.connected) cl.close();
			else std::cerr << "Not connected" << std::endl;
		}
		else if (*tok=="open")
		{
			if (cl.connected)
				std::cerr << "Already connected" << std::endl;
			else
			{
				std::string protocol=(++tok!=tokens.end())?*tok:"";
				cl.connect(protocol,"/");
			}
		}
		else if (*tok=="old")
		{
			if (cl.connected)
				std::cerr << "Already connected" << std::endl;
			else
			{
				std::string protocol=(++tok!=tokens.end())?*tok:"";
				cl.connect("",protocol);
			}
		}
		else if (*tok=="q")
		{
			if (cl.connected) cl.close();
			break;
		}
		else if (*tok=="send")
		{
			if (cl.connected)
			{
				std::string message;
				while (++tok!=tokens.end()) message+=(" "+*tok);
				auto reply=cl.send_get(std::string(message));
				std::cout << ">> " << reply << std::endl;
			}
			else std::cerr << "Not connected" << std::endl;
		}
		else
			std::cerr<<"What is [" << *tok << "]?!?" << std::endl;
	}
    return EXIT_SUCCESS;
}
