#include "tcp.hpp"
namespace FS=boost::filesystem;
class Parser {
    log4cplus::Logger& log_;
	std::mutex mtx_;
public:
	void on_error(TCP::Session& session,TCP::Protocol& ch)
	{
		LOG4CPLUS_INFO(log_,std::hex << std::this_thread::get_id() << std::dec << ":" << ch.name << ":error");
	};
	void on_open(TCP::Session& session,TCP::Protocol& ch)
	{
		LOG4CPLUS_INFO(log_,std::hex << std::this_thread::get_id() << std::dec << ":" << ch.name << ":open with "
			<< session.remote_endpoint.address().to_string() << "/" 
			<< session.remote_endpoint.port() << " at "
			<< session.local_endpoint.address().to_string() << "/" 
			<< session.local_endpoint.port());
	};
	void on_close(TCP::Session& session,TCP::Protocol& ch)
	{
		LOG4CPLUS_INFO(log_,std::hex << std::this_thread::get_id() << std::dec << ":" << ch.name << ":close");
	};
	void chat_terminal(TCP::Protocol& ch,TCP::Session& session,const std::string& message)
	{
		LOG4CPLUS_INFO(log_,std::hex << std::this_thread::get_id() << std::dec << ":" << ch.name << " << " << message);
		std::lock_guard<std::mutex> lock(mtx_);
		std::cout << std::hex << std::this_thread::get_id() << std::dec << ":" << ch.name << "<<" << message << std::endl;
		std::string reply;
		std::cout << std::hex << std::this_thread::get_id() << std::dec << ":" << "Enter reply:";
		std::getline(std::cin,reply);
		LOG4CPLUS_INFO(log_,std::hex << std::this_thread::get_id() << std::dec << ":" << ch.name << " >> " << reply);
		session.Send(reply);
	};
	void echoback(TCP::Protocol& ch,TCP::Session& session,const std::string& message)
	{
		LOG4CPLUS_INFO(log_,std::hex << std::this_thread::get_id() << std::dec << ":" << ch.name << " << " << message);
		session.Send(message);
	};
	void slow_echo(TCP::Protocol& ch,TCP::Session& session,const std::string& message)
	{
		LOG4CPLUS_INFO(log_,std::hex << std::this_thread::get_id() << std::dec << ":" << ch.name << " << " << message);
		std::this_thread::sleep_for(std::chrono::seconds(10));
		session.Send(message);
	};
	Parser(log4cplus::Logger& logger):log_(logger){
	};
};
int main(int argc, char* argv[])
{
    if (argc != 2) {
		std::cerr << "Usage: client <port>" << std::endl;
		return EXIT_FAILURE;
	}
	//Prepare logger
	FS::path home_folder=std::getenv("HOME");
	FS::path log4_file=home_folder/"log"/"tcp.cfg";
	if (!FS::exists(log4_file)) {
		std::cerr << "Can not find log4c++ configuration:" << log4_file.string() << std::endl;
		return EXIT_FAILURE;
	}
	log4cplus::PropertyConfigurator config(log4_file.string());
	config.configure();
	log4cplus::initialize();
	log4cplus::Logger log_=log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("tcp"));
	//
	unsigned int orig_port=std::atoi(argv[1]);
	unsigned int open_port=Check_Port(orig_port).GetPort();	//search open port
	if (orig_port!=open_port) std::cout << "Using port= " << open_port << std::endl;
	//
	TCP server(log_,open_port);
	Parser parser(log_);
	auto& def=server.Protocols[""];
	auto& echo=server.Protocols["echo"];
	auto& slow=server.Protocols["slowpoke"];
	auto& cmd=server.Protocols["cmd"];
	auto& chk=server.Protocols["chk"];
	auto& write=server.Protocols["write"];
	write.wait_message=false;
	//	write channel: server send a message -> client reply -> server send message ...
	//	other channel: client send a message -> server reply -> client send message ...
	//open action (if required)
	def.on_open		=[&parser,&def](TCP::Session& session){parser.on_open(session,def);};
	echo.on_open	=[&parser,&echo](TCP::Session& session){parser.on_open(session,echo);};
	slow.on_open	=[&parser,&slow](TCP::Session& session){parser.on_open(session,slow);};
	cmd.on_open		=[&parser,&cmd](TCP::Session& session){parser.on_open(session,cmd);};
	write.on_open	=[&log_,&write](TCP::Session& session){
		LOG4CPLUS_INFO(log_,std::hex << std::this_thread::get_id() << std::dec << ":" << write.name << ":open with "
			<< session.remote_endpoint.address().to_string() << "/" 
			<< session.remote_endpoint.port() << " at "
			<< session.local_endpoint.address().to_string() << "/" 
			<< session.local_endpoint.port());
		session.Send("Server send 1");
	};
	//close action (if required)
	echo.on_close	=[&parser,&echo](TCP::Session& session){parser.on_close(session,echo);};
	slow.on_close	=[&parser,&slow](TCP::Session& session){parser.on_close(session,slow);};
	cmd.on_close	=[&parser,&cmd](TCP::Session& session){parser.on_close(session,cmd);};
	//error action (if required)
	slow.on_error	=[&parser,&slow](TCP::Session& session,const boost::system::error_code& ec){parser.on_error(session,slow);};
	cmd.on_error	=[&parser,&cmd](TCP::Session& session,const boost::system::error_code& ec){parser.on_error(session,cmd);};
	//message action
	def.on_message=[&log_](TCP::Session& session,const std::string& message){
		LOG4CPLUS_INFO(log_,std::hex << std::this_thread::get_id() << std::dec << ":default << " << message);
		session.Send("I got "+message);
	};
	echo.on_message=[&parser,&echo](TCP::Session& session,const std::string& message){
		parser.echoback(echo,session,message);
	};
	slow.on_message=[&parser,&slow](TCP::Session& session,const std::string& message){
		parser.slow_echo(slow,session,message);
	};
	cmd.on_message=[&parser,&cmd](TCP::Session& session,const std::string& message){
		parser.chat_terminal(cmd,session,message);
	};
	chk.on_message=[&server,&cmd](TCP::Session& session,const std::string& message){
		if (message=="list")
			session.Send(server.list());
		else if (message=="quit")
		{
			session.Send("Terminating...");
			server.stop();
		}
		else
			session.Send("What is ["+message+"]?");
	};
	write.on_message=[&parser,&write](TCP::Session& session,const std::string& message){
		session.Send("Server got :"+message);
		session.Send("By the way, I am the leader of this connection.");
		session.Send("Thus I will send a message as I like. DO YOU UNDERSTAND?");
	};
	//OK let's go
    log4cplus::NDCContextCreator *_context;
    _context=new log4cplus::NDCContextCreator(LOG4CPLUS_TEXT("test_server"));
	LOG4CPLUS_INFO(log_,"* * * * Test-Server started * * * *");
	server.start();
	LOG4CPLUS_INFO(log_,"Test-Server ended");
	return EXIT_SUCCESS;
};
