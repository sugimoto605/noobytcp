//
//  tcp.cpp
//  mix3
//
//  Created by Hiroshi Sugimoto on 2016/04/14.
//  Copyright (c) 2016 Hiroshi Sugimoto. All rights reserved.
//
#include "tcp.hpp"
//
Check_Port::Check_Port(unsigned long port):sock_(ioc_),port_(port){};
unsigned long Check_Port::GetPort(){
	bool _fail=true;
	do {
		boost::asio::ip::tcp::acceptor AC_(ioc_);
		boost::asio::ip::tcp::endpoint EP_(boost::asio::ip::tcp::v4(),port_);
		boost::beast::error_code ec;
		AC_.open(EP_.protocol(),ec);	// Open the acceptor
		if(ec) {port_++;continue;}
		AC_.set_option(boost::asio::socket_base::reuse_address(true), ec);	// Allow address reuse
		if(ec) {port_++;continue;}
		AC_.bind(EP_,ec); 	// Bind to the server address
		if(ec) {port_++;continue;}
		_fail=false;
	} while (_fail);
	return port_++;
};
//
void TCP::Session::Close() {
	//Never invoke callback on_close() when server close the connection.
	//It is prepared for the case client close the connection, thus it will contain
	//TCP::Session::Close()
	ws_.close(boost::beast::websocket::close_code::normal);
	//Usually this will lead an error in do_read().
};
void TCP::Session::Fail(boost::beast::error_code ec, char const* what) {
	LOG4CPLUS_ERROR(parent_.log_,"Session:" << what << ":" << ec.message());
};
void TCP::Session::on_run() {// Start the asynchronous operation
	ws_.set_option(boost::beast::websocket::stream_base::timeout::suggested(boost::beast::role_type::server));
	ws_.set_option(boost::beast::websocket::stream_base::decorator(
		[](boost::beast::websocket::response_type& res) {
			res.set(boost::beast::http::field::server,std::string(BOOST_BEAST_VERSION_STRING) + " websocket-server-async");
		}
	));
	ws_.async_accept(req_,boost::beast::bind_front_handler(&TCP::Session::on_accept,shared_from_this()));
};
void TCP::Session::on_accept(boost::beast::error_code ec) {
	if(ec) return Fail(ec,"accept");
	if (protocol_.on_open) protocol_.on_open(*this);
	if (protocol_.wait_message) do_read();
};
void TCP::Session::do_read() {
	ws_.async_read(buffer_,boost::beast::bind_front_handler(&TCP::Session::on_read,shared_from_this()));
};
void TCP::Session::on_read(boost::beast::error_code ec, std::size_t bytes_transferred) {
	boost::ignore_unused(bytes_transferred);
	if(ec == boost::beast::websocket::error::closed)// This indicates that the session was closed
	{
		if (protocol_.on_close) protocol_.on_close(*this);
		parent_.sessions_.erase(shared_from_this());
		return;
	}
	if(ec)
	{
		if (protocol_.on_error) protocol_.on_error(*this,ec);
		parent_.sessions_.erase(shared_from_this());
		return;
	}
	ws_.text(ws_.got_text());
	if (protocol_.on_message)
	{
		std::string message_=boost::beast::buffers_to_string(buffer_.data());
		protocol_.on_message(*this,message_);
	}
	buffer_.consume(buffer_.size()); // Clear the buffer
	if (protocol_.wait_message) do_read();
};
void TCP::Session::on_write(boost::beast::error_code ec, std::size_t bytes_transferred) {
	boost::ignore_unused(bytes_transferred);
	if(ec) return Fail(ec, "write");
	buffer_.consume(buffer_.size()); // Clear the buffer
	do_read();
};
void TCP::Session::Run() {
	boost::asio::dispatch(ws_.get_executor(),
		boost::beast::bind_front_handler(&Session::on_run,shared_from_this()));
};
void TCP::Session::Read(){
	buffer_.consume(buffer_.size()); // Clear the buffer
	do_read();
};
void TCP::Session::Send(const std::string& message){
	ws_.write(boost::asio::buffer(message));
};
TCP::Listener::Listener(boost::asio::io_context& ioc,boost::asio::ip::tcp::endpoint endpoint,TCP& parent):ioc_(ioc),acceptor_(ioc),parent_(parent){
	acceptor_.open(endpoint.protocol(),ec);	// Open the acceptor
	if(ec) throw(std::runtime_error("TCP::Listener::open"));
	acceptor_.set_option(boost::asio::socket_base::reuse_address(true), ec);	// Allow address reuse
	if(ec) throw(std::runtime_error("TCP::Listener::set_option"));
	acceptor_.bind(endpoint, ec); 	// Bind to the server address
	if(ec) throw(std::runtime_error("TCP::Listener::bind"));
	acceptor_.listen(boost::asio::socket_base::max_listen_connections, ec);	// Start listening for connections
	if(ec) throw(std::runtime_error("TCP::Listener::listen"));
};
void TCP::Listener::run(){
	do_accept();
};
void TCP::Listener::do_accept(){
	acceptor_.async_accept(boost::asio::make_strand(ioc_),
		boost::beast::bind_front_handler(&TCP::Listener::on_accept, shared_from_this()));
}
void TCP::Listener::on_accept(boost::beast::error_code ec, boost::asio::ip::tcp::socket socket){
	if(ec) throw(std::runtime_error("TCP::Listener::accept"));
	// Inspect header
	bool accept_;
	auto sp_it=parent_.Protocols.end();
	boost::beast::http::request<boost::beast::http::string_body> req;//requestを入れる場所を確保
	boost::beast::flat_buffer req_buffer;
	try {
		boost::beast::http::read(socket,req_buffer,req);//ここでreqに色々入る
		accept_=boost::beast::websocket::is_upgrade(req);//upgradeリクエストである
	}
	catch(std::exception& e){
		LOG4CPLUS_ERROR(parent_.log_,"NoobyTCP::Listener::on_accept::Error on read:" << e.what());
		accept_=false;
	}
	if (accept_)
	{
		LOG4CPLUS_INFO(parent_.log_,"NoobyTCP::Listener::on_accept::Accepted " << req.target());
		//LOG4CPLUS_INFO(parent_.log_,"\t" << "target = " << req.target());
		//for(auto const& field:req)
		//{
		//	LOG4CPLUS_INFO(parent_.log_,"\t" << field.name() << " = " << field.value());
		//}
		std::string sp_name="";
		auto it=req.find("Sec-WebSocket-Protocol");
		if (it!=req.end()) sp_name=std::string(it->value().data(),it->value().size());
		//Sec-ヘッダーが無かった時代のWebSocketクライアントは, target部分にサブプロトコルを入れて送ってくる
		//		旧WebSocketクライアント		URI=http://host:port/sub-protocol
		//		新WebSocketクライアント		URI=http://host:port/target
		//					target					=target
		//					Sec-WebScoket-Protocol	=sub-protocol
		if (sp_name=="")
		{
			sp_name=req.target().to_string();
			boost::erase_all(sp_name,"/");
		}
		//クライアントがsub-protocol(名称sp_name)を指定している
		//	クライアントは, 利用するsub-protocolの一覧を echo, log, commit という感じで送ってくる
		//	どう使うのか分からないので, とりあえず, 単一のプロトコルを送ってくる場合だけ回線を開く
		if (((sp_it=parent_.Protocols.find(sp_name))!=parent_.Protocols.end()))
		{
			auto session_ptr=std::make_shared<TCP::Session>(std::move(socket),req,sp_it->second,parent_);
			parent_.sessions_[session_ptr]=&(sp_it->second);
			session_ptr->Run();
		}
		else {//いや, 一応，クライアントにエラーしたよって伝えたい
			boost::beast::http::response<boost::beast::http::string_body> res;//responseを入れる場所を確保
			res.version(11);
			res.set(boost::beast::http::field::server, "WebMix/TCP");
			res.result(boost::beast::http::status::bad_request);
			res.set(boost::beast::http::field::content_type, "text/plain");
			res.body() = "Not  upgrade request or unknown sub-protocol.";
			res.prepare_payload();
			try {
				boost::beast::http::write(socket,res);//ここでreqに色々入る
			}
			catch(std::exception& e){
				LOG4CPLUS_ERROR(parent_.log_,"Error on write:" << e.what());
			}
		}
	}
	do_accept();	// Accept next connection
};
TCP::TCP(log4cplus::Logger& logger,unsigned short port,unsigned short threads):ioc_(threads),threads_(threads),log_(logger){
	//デフォルトのendpoint(address 0.0.0.0で, なんでも受け取るサーバー)
	auto endpoint = boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port);
    std::make_shared<Listener>(ioc_,endpoint,*this)->run(); // Create and run a listening port
	auto& cmd=Protocols[""];//Define default protocol
};
std::string TCP::list(){
	std::string reply="active sessions:\n";
	for(auto [session_ptr,protocol_ptr]:sessions_)
		reply+=session_ptr->remote_endpoint.address().to_string()+":"+std::to_string(session_ptr->remote_endpoint.port())
			+"("+protocol_ptr->name+")\n";
	return reply;
};
void TCP::start(std::string NDC_layer){
	for(auto& [name,protocol]:Protocols) protocol.name=name;
	if (NDC_layer!="") log4cplus::NDCContextCreator _context=log4cplus::NDCContextCreator(LOG4CPLUS_TEXT(NDC_layer));
	auto Stack=log4cplus::getNDC().cloneStack();
    std::vector<std::thread> v;
    v.reserve(threads_ - 1);
    for(auto i = threads_ - 1; i > 0; --i) v.emplace_back([this,&Stack]{
		log4cplus::getNDC().inherit(Stack);
		ioc_.run();
	});
    ioc_.run();
    for(auto& th:v) th.join();
};
void TCP::stop(){
	for(auto [session_ptr,protocol_ptr]:sessions_) session_ptr->Close();
	sessions_.clear();
    ioc_.stop();
};
